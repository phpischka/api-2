<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\TaskController;
use App\Http\Controllers\TagController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

//Tasks
Route::get('/get/tasks', [TaskController::class, 'tasks']);
Route::get('/get/task/{id}', [TaskController::class, 'task']);
Route::post('/create/task', [TaskController::class, 'create']);
//Tags
Route::get('/get/tags', [TagController::class, 'tags']);
Route::get('/get/tag/{id}', [TagController::class, 'tag']);
Route::post('/create/tag', [TagController::class, 'create']);
